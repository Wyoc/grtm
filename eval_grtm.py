from gRTM.grtm import GRTM
import pickle
import numpy as np
import sys
from gRTM.utils import id_sent_to_dict, links_location, group_by_links
from random import sample, randint, choice
from collections import defaultdict
import os


CORPUS = sys.argv[1]

if not os.path.exists(f'./datas/{CORPUS}_D1_sent/'):
    os.makedirs(f'./datas/{CORPUS}_D1_sent/')

doc_ids = pickle.load(open(f'./corpora/{CORPUS}_D1_sent/doc_ids.pkl', 'rb'))
doc_cnt = pickle.load(open(f'./corpora/{CORPUS}_D1_sent/doc_cnt.pkl', 'rb'))
doc_links = pickle.load(open(f'./corpora/{CORPUS}_D1_sent/links.pkl', 'rb'))
voca = pickle.load(open(f'./corpora/{CORPUS}_D1_sent/vocab.pkl', 'rb'))
link_per_word_sent = pickle.load(open(f'./corpora/{CORPUS}_D1_sent/links_per_word_sent.pkl', 'rb'))

doc_sent_words = group_by_links(link_per_word_sent)
doc_ids_sent_ordered = pickle.load(open(f'./corpora/{CORPUS}_D1_sent/doc_ids_sent_ordered.pkl', 'rb'))    

new_doc_sent_words = doc_sent_words.copy()

dict_ids_sent_ordered = id_sent_to_dict(doc_ids_sent_ordered)

D = len(doc_ids)
n_removed = int(D)

id_doc_to_cut = sample(range(0, len(doc_ids)), n_removed)
id_doc_dev = id_doc_to_cut[0:int(len(id_doc_to_cut)/2)]
id_doc_test = id_doc_to_cut[int(len(id_doc_to_cut)/2):]

removed_links = defaultdict(dict)
removed_links_word = defaultdict(dict)

removed_links_dev = defaultdict(dict)
removed_links_word_dev = defaultdict(dict)

removed_links_test = defaultdict(dict)
removed_links_word_test = defaultdict(dict)

for d in id_doc_to_cut:
    # if new_doc_sent_words[d]:
    random_sentence = choice(list(new_doc_sent_words[d].keys()))
    if new_doc_sent_words[d][random_sentence]:
        random_linked_doc = choice(list(new_doc_sent_words[d][random_sentence].keys()))
        removed_links_word[d][random_linked_doc] = new_doc_sent_words[d][random_sentence][random_linked_doc]
        if d in id_doc_dev:
            removed_links_word_dev[d][random_linked_doc] = new_doc_sent_words[d][random_sentence][random_linked_doc]
            removed_links_dev[d] = random_linked_doc
        else:
            removed_links_word_test[d][random_linked_doc] = new_doc_sent_words[d][random_sentence][random_linked_doc]
            removed_links_test[d] = random_linked_doc
        new_doc_sent_words[d][random_sentence].pop(random_linked_doc)
        removed_links[d] = random_linked_doc

# Generate new link_per_sent array for sent_RTM with removed links
new_link_per_sent = []
for d in new_doc_sent_words.keys():
    new_link_per_sent.append([])
    for s in new_doc_sent_words[d].keys():
        new_link_per_sent[d].append(list(new_doc_sent_words[d][s].keys()))

# Generate new doc_link array for RTM with removed links
new_doc_link = []
for d in new_doc_sent_words.keys():
    l = []
    for s in new_doc_sent_words[d].keys():
        l.append(list(new_doc_sent_words[d][s].keys()))
    new_doc_link.append([v for array in l for v in array])

# links per word for ITRTM
new_link_per_word = []
for docid in new_doc_sent_words.keys():
    doc_array = []
    for sentid in new_doc_sent_words[docid].keys():
        for aid in new_doc_sent_words[docid][sentid].keys():
            for word in new_doc_sent_words[docid][sentid][aid]:
                doc_array.append([word, aid])
    new_link_per_word.append(doc_array)

new_links_word_location = links_location(new_doc_sent_words, dict_ids_sent_ordered)

pickle.dump(removed_links, open(f'./datas/{CORPUS}_D1_sent/removed_links.pkl', 'wb'))    
pickle.dump(removed_links_word, open(f'./datas/{CORPUS}_D1_sent/removed_links_word.pkl', 'wb'))    
pickle.dump(id_doc_to_cut, open(f'./datas/{CORPUS}_D1_sent/id_doc_to_cut.pkl', 'wb'))    
pickle.dump(new_doc_sent_words, open(f'./datas/{CORPUS}_D1_sent/new_doc_sent_words.pkl', 'wb'))    
pickle.dump(new_link_per_sent, open(f'./datas/{CORPUS}_D1_sent/new_link_per_sent.pkl', 'wb'))    
pickle.dump(new_doc_link, open(f'./datas/{CORPUS}_D1_sent/new_doc_link.pkl', 'wb'))    
pickle.dump(new_link_per_word, open(f'./datas/{CORPUS}_D1_sent/new_link_per_word.pkl', 'wb'))    
pickle.dump(new_links_word_location, open(f'./datas/{CORPUS}_D1_sent/new_links_word_location.pkl', 'wb'))    
pickle.dump(dict_ids_sent_ordered, open(f'./datas/{CORPUS}_D1_sent/dict_ids_sent_ordered', 'wb'))    

def build_docterm_matrix(doc_ids, doc_cnt, len_voca):
    ndoc = len(doc_ids)
    docterm_matrix = np.zeros((ndoc, len_voca), dtype=int)
    for doc_idx, doc in enumerate(doc_ids):
        for word_idx, word in enumerate(doc):
            docterm_matrix[doc_idx, word] = int(doc_cnt[doc_idx][word_idx])
    #docterm_matrix = docterm_matrix.T
    return docterm_matrix

def build_y(doc_links, n_doc):
    y = []
    for doc_idx, links in enumerate(doc_links):
        for link in links:
            y.append(np.array([doc_idx, link, 1]))
    for i in range(0,len(doc_links)):
        d1 = np.random.randint(0,n_doc)
        d2 = np.random.randint(0,n_doc)
        y.append(np.array([d1, d2, 0]))
    np.random.shuffle(y)
    return np.array(y)

y = build_y(new_doc_link, D)

docterm_matrix = build_docterm_matrix(doc_ids, doc_cnt, len(voca))

mu = 0.
nu2 = 1.
V = len(voca)
K = 50
alpha = np.ones(K)
_alpha = alpha[:K]
_beta = np.repeat(0.01, V)
_mu = mu
_nu2 = nu2
_b = 1.
n_iter = 800
grtm = GRTM(K, _alpha, _beta, _mu, _nu2, _b, n_iter)

grtm.fit(docterm_matrix, y)

pickle.dump(grtm,open(f'./models/grtm_{CORPUS}.pkl', 'wb'))
