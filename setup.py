# python setup.py build_ext --inplace

import os

from setuptools import setup
from setuptools.extension import Extension

import numpy as np
import cython_gsl
try:
    from Cython.Build import cythonize
    USE_CYTHON = True
except ImportError:
    USE_CYTHON = False

# Dealing with Cython
#USE_CYTHON = os.environ.get('USE_CYTHON', True)
ext = '.pyx' if USE_CYTHON else '.c'

extensions = [
    Extension('gRTM._grtm', ["gRTM/_grtm.pyx"],           
                                        libraries=cython_gsl.get_libraries(),
                                        library_dirs=[cython_gsl.get_library_dir()],
                                        include_dirs=[cython_gsl.get_cython_include_dir()])
]

if USE_CYTHON:
    print('---*--- USING CYTHON ---*---')
    setup(ext_modules = cythonize(extensions),)