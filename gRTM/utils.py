import numpy as np
import matplotlib.pyplot as plt

import io
from six.moves import xrange
import timeit
from collections import defaultdict
from scipy import signal

def timer(function):
  def new_function():
    start_time = timeit.default_timer()
    function()
    elapsed = timeit.default_timer() - start_time
    print('Function "{name}" took {time} seconds to complete.'.format(name=function.__name__, time=elapsed))
  return new_function()

def sampling_from_dist(prob):
    """ Sample index from a list of unnormalised probability distribution
        same as np.random.multinomial(1, prob/np.sum(prob)).argmax()

    Parameters
    ----------
    prob: ndarray
        array of unnormalised probability distribution

    Returns
    -------
    new_topic: return a sampled index
    """
    thr = prob.sum() * np.random.rand()
    new_topic = 0
    tmp = prob[new_topic]
    while tmp < thr:
        new_topic += 1
        tmp += prob[new_topic]
    return new_topic


def sampling_from_dict(prob):
    """ sample key from dictionary `prob` where values are unnormalised probability distribution

    Parameters
    ----------
    prob: dict
        key = topic
        value = unnormalised probability of the topic

    Returns
    -------
    key: int
        sampled key
    """
    prob_sum = sum(prob.values())

    thr = prob_sum * np.random.rand()
    tmp = 0
    for key, p in prob.items():
        tmp += p
        if tmp < thr:
            new_topic = key
    return new_topic


def isfloat(value):
    """
    Check the value is convertable to float value
    """
    try:
        float(value)
        return True
    except ValueError:
        return False


def read_voca(path):
    """
    open file from path and read each line to return the word list
    """
    with open(path, 'r') as f:
        return [word.strip() for word in f.readlines()]


def word_cnt_to_bow_list(word_ids, word_cnt):
    corpus_list = list()
    for di in xrange(len(word_ids)):
        doc_list = list()
        for wi in xrange(len(word_ids[di])):
            word = word_ids[di][wi]
            for c in xrange(word_cnt[di][wi]):
                doc_list.append(word)
        corpus_list.append(doc_list)
    return corpus_list


def log_normalize(log_prob_vector):
    """
    returns a probability vector of log probability vector
    """
    max_v = log_prob_vector.max()
    log_prob_vector += max_v
    log_prob_vector = np.exp(log_prob_vector)
    log_prob_vector /= log_prob_vector.sum()
    return log_prob_vector


def convert_cnt_to_list(word_ids, word_cnt):
    corpus = list()

    for di in xrange(len(word_ids)):
        doc = list()
        doc_ids = word_ids[di]
        doc_cnt = word_cnt[di]
        for wi in xrange(len(doc_ids)):
            word_id = doc_ids[wi]
            for si in xrange(doc_cnt[wi]):
                doc.append(word_id)
        corpus.append(doc)
    return corpus


def write_top_words(topic_word_matrix, vocab, filepath, n_words=20, delimiter=',', newline='\n'):
    with open(filepath, 'w') as f:
        for ti in xrange(topic_word_matrix.shape[0]):
            top_words = vocab[topic_word_matrix[ti, :].argsort()[::-1][:n_words]]
            f.write('%d' % (ti))
            for word in top_words:
                f.write(delimiter + word)
            f.write(newline)


def get_top_words(topic_word_matrix, vocab, topic, n_words=20):
    if not isinstance(vocab, np.ndarray):
        vocab = np.array(vocab)
    top_words = vocab[topic_word_matrix[topic].argsort()[::-1][:n_words]]
    return top_words

def show_elbo(model, max_iter):
    plt.plot(list(range(max_iter)), model.elbo)
    plt.title('ELBO')
    plt.xlabel('step')
    plt.show()

def sample_links(l, n):
    from random import sample
    samples = []
    edgelist = []
    [edgelist.append([idx, link]) for idx, links in enumerate(l) for link in links]
    [samples.append(edgelist[idx]) for idx in sample(range(0, len(edgelist)), n)]
    return samples

def make_edgelist(nested_list):
    edgelist = []
    for idx, links in enumerate(nested_list):
        for link in links:
            edgelist.append([idx, link])
    return edgelist

def make_symetrical(array):
    len_input = len(array)
    sym_list = np.zeros(2*len_input -1)
    for i, value in enumerate(array):
        sym_list[len_input-i-1] = value
        sym_list[len_input+i-1] = value
    return sym_list

def make_gaussian_mask(word_loc, len_sentence, std=3):
    if len_sentence - word_loc>word_loc:
        gaussian = signal.gaussian(2*(len_sentence - word_loc), std=std)
        return gaussian[-len_sentence:]
    else:
        gaussian = signal.gaussian(2*(word_loc+1), std=std)
        return gaussian[:len_sentence]

def links_location(doc_sent_words, dict_ids_sent_ordered):
    word_link_location = defaultdict(dict)
    for docid in doc_sent_words.keys():
        for sentid in doc_sent_words[docid].keys():
            word_link_location[docid][sentid] = {}
            for linked_doc in doc_sent_words[docid][sentid].keys():
                word_idx = dict_ids_sent_ordered[docid][sentid].index(doc_sent_words[docid][sentid][linked_doc][0])
                word_link_location[docid][sentid][linked_doc] = word_idx
    return word_link_location  

def id_sent_to_dict(array):
    ordered_words_dict = defaultdict(dict)
    for docid, doc in enumerate(array):
        for sentid, sent in enumerate(doc):
            ordered_words_dict[docid][sentid] = sent
    return ordered_words_dict

def link_word_location_in_doc(doc_ids_sent_ordered, links_word_location):
    words_link_in_doc = defaultdict(dict)
    for docid, doc in enumerate(doc_ids_sent_ordered):
        nb_words = 0
        for sentid, sent in enumerate(doc_ids_sent_ordered[docid]):
            if links_word_location[docid][sentid]:
                for adi in links_word_location[docid][sentid]:
                    word_location = links_word_location[docid][sentid][adi]
                    words_link_in_doc[docid][adi] = nb_words + word_location
            nb_words += len(sent)
    return words_link_in_doc

def order_ids(doc_ids_sent_ordered):
    doc_id_ordered = defaultdict(dict)
    for docid, doc in enumerate(doc_ids_sent_ordered):
        doc_id_ordered[docid] = []
        for sentid, sent in enumerate(doc):
            doc_id_ordered[docid].extend(sent)
    return doc_id_ordered

def group_by_links(docs):
    grouped_links = {}
    for iddoc, doc in enumerate(docs):
        grouped_links[iddoc] = {}
        for idsent, sent in enumerate(doc):
            values = set(map(lambda x:x[1], sent))
            grouped_links[iddoc][idsent] = {}
            for x in values:
                v = [y[0] for y in sent if y[1]==x]
                grouped_links[iddoc][idsent][x] = v
    return grouped_links


def load_vectors(fname):
    fin = io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
    n, d = map(int, fin.readline().split())
    data = {}
    for line in fin:
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = map(float, tokens[1:])
    return data

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

def test_word_rank(predicted_words, doc_word_links):
    total_rank = 0
    for source_doc in predicted_words.keys():
        for target_doc in predicted_words[source_doc].keys():
            total_rank += predicted_words[source_doc][target_doc][::-1].index(doc_word_links[source_doc][target_doc][0])
            #total_rank += np.where(predicted_words[source_doc][target_doc][::-1] == doc_word_links[source_doc][target_doc][0])[0][0]
    return total_rank

def group_by_links(docs):
    grouped_links = {}
    for iddoc, doc in enumerate(docs):
        grouped_links[iddoc] = {}
        for idsent, sent in enumerate(doc):
            values = set(map(lambda x:x[1], sent))
            grouped_links[iddoc][idsent] = {}
            for x in values:
                v = [y[0] for y in sent if y[1]==x]
                grouped_links[iddoc][idsent][x] = v
    return grouped_links

def total_links(predicted_words):
    total = 0
    for source_doc in predicted_words.keys():
        for target_doc in predicted_words[source_doc].keys():
            total +=1   
    return total