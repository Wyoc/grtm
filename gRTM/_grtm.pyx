#cython: language_level=3
#cython: boundscheck=False
#cython: wraparound=False
#cython: cdivision=True

"""
The heavy-lifting is here in cython.

Draws from Allen Riddell's LDA library https://github.com/ariddell/lda
"""

from datetime import (datetime, timedelta)
import numpy as np
from libc.math cimport fabs
from cython.operator cimport (preincrement, predecrement)
from cython_gsl cimport (gsl_sf_lngamma as lngamma, gsl_sf_exp as exp,
                         gsl_sf_log as ln, gsl_rng, gsl_rng_mt19937,
                         gsl_rng_alloc, gsl_rng_set,
                         gsl_rng_uniform, gsl_rng_uniform_int,
                         gsl_ran_gaussian as gaussian)
from pypolyagamma import PyPolyaGamma


# we choose this number since it is a large prime
cdef unsigned int n_rands = 1000003
cdef gsl_rng *r =  gsl_rng_alloc(gsl_rng_mt19937)


cdef double[:] create_rands(unsigned int n_rands, seed=None):
    """
    Create array of uniformly random numbers on the interval [0, 1).
    """

    cdef:
        int i
        double[::1] rands = np.empty(n_rands, dtype=np.float64, order='C')
    if seed is not None:
        gsl_rng_set(r, seed)
    for i in range(n_rands):
        rands[i] = gsl_rng_uniform(r)
    return rands


cdef int[:] create_topic_lookup(unsigned int n_tokens, unsigned int n_topics,
                                seed=None):
    """
    Create array of uniformly random numbers on the interval [0, 1).
    """

    cdef:
        int i
        int[::1] topic_lookup = np.empty(n_tokens, dtype=np.intc, order='C')
    if seed is not None:
        gsl_rng_set(r, seed)
    for i in range(n_tokens):
        topic_lookup[i] = gsl_rng_uniform_int(r, n_topics)
    return topic_lookup


cdef int searchsorted(double[:] a, double v):
    """
    Find indices where elements should be inserted to maintain order.

    Find the indices into a sorted array a such that, if the corresponding
    elements in v were inserted before the indices, the order of a would be
    preserved.

    Like numpy.searchsorted
    (http://docs.scipy.org/doc/numpy/reference/generated/numpy.searchsorted.html).
    """

    cdef:
        int imin = 0
        int imax = a.shape[0]
        int imid
    while imin < imax:
        imid = imin + ((imax - imin) >> 1)
        if v > a[imid]:
            imin = imid + 1
        else:
            imax = imid
    return imin


cdef double loglikelihood_grtm(int[:, :] nzw, int[:, :] ndz, int[:] nz,
                               double[:] alpha, double[:] beta, double sum_beta,
                               double mu, double nu2, double b,
                               double[:, :] H, double[:] y, double[:] zeta):
    """
    Log likelihood calculation for generalized relational topic models with
    data augmentation.

    This is not an exact calculation (constants not included).
    """

    cdef:
        int k, _k, d
        int n_docs = ndz.shape[0]
        int n_topics = ndz.shape[1]
        int n_terms = nzw.shape[1]
        int n_edges = y.shape[0]
        double ll = 0.
    # calculate log p(w|z) and log p(H)
    for k in range(n_topics):
        ll -= lngamma(sum_beta + nz[k])
        for _k in range(n_topics):
            ll -= (H[k, _k] - mu) * (H[k, _k] - mu) / 2 / nu2
        for w in range(n_terms):
            ll += lngamma(beta[w] + nzw[k, w])
    # calculate log p(z)
    for d in range(n_docs):
        for k in range(n_topics):
            ll += lngamma(alpha[k] + ndz[d, k])
    # calculate log p(y|H, z)
    for e in range(n_edges):
        ll += b * (y[e] * zeta[e] - ln(1 + exp(zeta[e])))
    return ll

cdef print_progress(start_time, int n_report_iter, int i,
                    double lL_now, double lL_last):
    """
    Print progress of iterations.
    """

    if i > 0 and i % n_report_iter == 0:
        now_time = datetime.now()
        print('{} {} elapsed, iter {:>4}, LL {:.4f}, {:.2f}% change from last'
            .format(now_time,
                    now_time - start_time,
                    i,
                    lL_now,
                    (lL_now - lL_last) / fabs(lL_last) * 100))


def estimate_matrix(int[:, :] counts, double[:] psuedo_counts, int n_things):
    """
    Create estimates for theta and phi from counts.
    """

    mat = np.asarray(counts) + np.tile(psuedo_counts, (n_things, 1))
    return (mat.T / mat.sum(axis=1)).T


def iterated_pseudo_counts(doc_lookup, term_lookup, int n_docs,
                           double[:] alpha, double[:] beta, double[:, :] phi,
                           int max_iter, double tol):
    """
    Estimate the topic distributions of new documents using the
    iterated pseudo-counts method mentioned in Wallach et al. (2009) and
    derived in Buntine (2009).
    """

    cdef:
        int d, i, k, s, w, n_tokens_d
        int n_topics = phi.shape[0]
        int[::1] term_lookup_d
        double sum_over_k, _tmp_double
        double[::1] q_sum
        double[:, ::1] q, q_new
        double[:, ::1] theta = np.empty((n_docs, n_topics), dtype=np.float64, order='C')
    for d in range(n_docs):
        term_lookup_d = np.ascontiguousarray(term_lookup[doc_lookup == d])
        n_tokens_d = term_lookup_d.shape[0]
        # initialize proposal distribution q
        q = np.empty((n_tokens_d, n_topics), dtype=np.float64, order='C')
        for i in range(n_tokens_d):
            w = term_lookup_d[i]
            sum_over_k = 0.
            for k in range(n_topics):
                _tmp_double = phi[k, w] * alpha[k]
                q[i, k] = _tmp_double
                sum_over_k += _tmp_double
            for k in range(n_topics):
                q[i, k] /= sum_over_k
        # do fixed point iteration
        q_new = np.empty_like(q, dtype=np.float64, order='C')
        for s in range(max_iter):
            # q_sum is
            q_sum = np.zeros(n_topics, dtype=np.float64, order='C')
            for i in range(n_tokens_d):
                for k in range(n_topics):
                    q_sum[k] += q[i, k]
            for i in range(n_tokens_d):
                w = term_lookup_d[i]
                sum_over_k = 0.
                for k in range(n_topics):
                    _tmp_double = phi[k, w] * (alpha[k] + q_sum[k] - q[i, k])
                    q_new[i, k] = _tmp_double
                    sum_over_k += _tmp_double
                for k in range(n_topics):
                    q_new[i, k] /= sum_over_k
            # return if difference between iterations is small
            sum_over_k = 0.
            for i in range(n_tokens_d):
                for k in range(n_topics):
                    sum_over_k += fabs(q_new[i, k] - q[i, k])
            # set q here in case we break
            q = q_new
            if sum_over_k < tol:
                break
        # calculate topic distributions
        q_sum = np.zeros(n_topics, dtype=np.float64, order='C')
        sum_over_k = 0.
        for i in range(n_tokens_d):
            for k in range(n_topics):
                q_sum[k] += q[i, k]
                sum_over_k += q[i, k]
        for k in range(n_topics):
            theta[d, k] = q_sum[k] / sum_over_k
    return np.array(theta)

def gibbs_sampler_grtm(int n_iter, int n_report_iter,
                       int n_topics, int n_docs,
                       int n_terms, int n_tokens, int n_edges,
                       double[:] alpha, double[:] beta,
                       double mu, double nu2, double b,
                       int[:] doc_lookup, int[:] term_lookup,
                       int[:] out_docs, int[:] out_edges,
                       int[:] in_docs, int[:] in_edges,
                       int[:] edge_tail, int[:] edge_head,
                       double[:] y, seed):
    """
    Perform collapsed Gibbs sampling inference for relational topic models
    using Polson et al.'s data augmentation strategy[1] and Zhu et al.'s
    regularization strategy[2].

    1. Polson, N. G., Scott, J. G., & Windle, J. (2013). Bayesian Inference
    for Logistic Models Using Pólya–Gamma Latent Variables. Journal of the
    American Statistical Association, 108(504), 1339–1349.
    http://doi.org/10.1080/01621459.2013.829001

    2. Chen, N., Zhu, J., Xia, F., & Zhang, B. (2013). Generalized relational topic
    models with data augmentation. Presented at the IJCAI'13: Proceedings of the
    Twenty-Third international joint conference on Artificial Intelligence,  AAAI
    Press.
    """

    cdef:
        int i, j, k, k1, k2, d, d1, d2, e, w, z, new_z, n, Nd
        double p_sum, uval, H_col_sum, H_row_sum, kappa_sum, zeta_sum
        double sum_alpha = 0.
        double sum_beta = 0.
        int[:] topic_lookup = create_topic_lookup(n_tokens, n_topics, seed)
        # log likelihoods
        double[::1] lL = np.empty(n_iter, dtype=np.float64, order='C')
        # number of tokens in document d assigned to topic z, shape = (n_docs, n_topics)
        int[:, ::1] ndz = np.zeros((n_docs, n_topics), dtype=np.intc, order='C')
        # number of tokens assigned to topic z equal to term w, shape = (n_topics, n_terms)
        int[:, ::1] nzw = np.zeros((n_topics, n_terms), dtype=np.intc, order='C')
        # number of tokens assigned to topic k, shape = (n_topics,)
        int[::1] nz = np.zeros(n_topics, dtype=np.intc, order='C')
        # number of tokens in doc d, shape = (n_docs,)
        int[::1] nd = np.zeros(n_docs, dtype=np.intc, order='C')
        # (weighted) probabilities for the discrete distribution
        double[::1] p_cumsum = np.empty(n_topics, dtype=np.float64, order='C')
        # preallocate uniformly random numbers on the interval [0, 1)
        double[:] rands = create_rands(n_rands=n_rands, seed=seed)
        int u = 0
        # zeta: TODO
        double[::1] zeta = np.empty(n_edges, dtype=np.float64, order='C')
        # regression coefficients
        double[:, :, ::1] H = np.ascontiguousarray(
            np.tile(mu, (n_iter + 1, n_topics, n_topics)), dtype=np.float64)
        # 0 = row, 1 = column
        double[:, ::1] Hznd = np.empty((n_edges, n_topics), dtype=np.float64, order='C')
        double[:, ::1] HTznd = np.empty((n_edges, n_topics), dtype=np.float64, order='C')
        double[::1] eta_mean = np.empty(n_topics * n_topics, dtype=np.float64, order='C')
        # omega: notice Im initializing omega here
        double[::1] omega = np.ascontiguousarray(np.repeat(1., n_edges))
        # kappa: a transformation of y
        double[::1] kappa = b * (np.asarray(y) - 0.5)
        # z by token
        double [:, ::1] zs = np.zeros((n_topics, n_tokens), dtype=np.float64, order='C')
    # initialize counts
    for j in range(n_tokens):
        preincrement(ndz[doc_lookup[j], topic_lookup[j]])
        preincrement(nzw[topic_lookup[j], term_lookup[j]])
        preincrement(nz[topic_lookup[j]])
        preincrement(nd[doc_lookup[j]])
    # initialize sum_alpha, lBeta_alpha
    for k in range(n_topics):
        sum_alpha += alpha[k]
    # initialize sum_beta, lBeta_beta
    for w in range(n_terms):
        sum_beta += beta[w]
    # define numpy variables
    Inu2 = np.identity(n_topics * n_topics) / nu2
    munu2 = np.repeat(mu / nu2, n_topics * n_topics)
    # define PolyaGamma sampler
    pg_rng = PyPolyaGamma(seed=seed or 42)
    # iterate
    start_time = datetime.now()
    print('{} start iterations'.format(start_time))
    for i in range(n_iter):
        # sample omega, and initialize Hznd and zeta for iteration i
        for e in range(n_edges):
            d1 = edge_tail[e]
            d2 = edge_head[e]
            Nd = nd[d1] * nd[d2]
            zeta_sum = 0.
            for k1 in range(n_topics):
                H_row_sum = 0.
                H_col_sum = 0.
                for k2 in range(n_topics):
                    zeta_sum += ndz[d1, k1] * ndz[d2, k2] * H[i, k1, k2]
                    H_row_sum += H[i, k1, k2] * ndz[d2, k2]
                    H_col_sum += H[i, k2, k1] * ndz[d1, k2]
                Hznd[e, k1] = H_row_sum / Nd
                HTznd[e, k1] = H_col_sum / Nd
            zeta[e] = zeta_sum / Nd
            omega[e] = pg_rng.pgdraw(b, zeta[e])
        # sample z
        for j in range(n_tokens):
            d = doc_lookup[j]
            w = term_lookup[j]
            z = topic_lookup[j]
            predecrement(ndz[d, z])
            predecrement(nzw[z, w])
            predecrement(nz[z])
            p_sum = 0.
            for k in range(n_topics):
                kappa_sum = 0.
                for n in range(in_docs[d], in_docs[d + 1]):
                    e = in_edges[n]
                    d1 = edge_tail[e]
                    Nd = nd[d1] * nd[d]
                    zeta_sum = zeta[e]
                    for k1 in range(n_topics):
                        zeta_sum -= ndz[d1, k1] * H[i, k1, z] / Nd
                    kappa_sum += (kappa[e] - omega[e] * zeta_sum
                                  - omega[e] / 2 * HTznd[e, k]) * HTznd[e, k]
                for n in range(out_docs[d], out_docs[d + 1]):
                    e = out_edges[n]
                    d2 = edge_head[e]
                    Nd = nd[d] * nd[d2]
                    zeta_sum = zeta[e]
                    for k2 in range(n_topics):
                        zeta_sum -= ndz[d2, k2] * H[i, z, k2] / Nd
                    kappa_sum += (kappa[e] - omega[e] * zeta_sum
                                  - omega[e] / 2 * Hznd[e, k]) * Hznd[e, k]
                p_sum += (nzw[k, w] + beta[w]) \
                    / (nz[k] + sum_beta) \
                    * (ndz[d, k] + alpha[k]) \
                    * exp(kappa_sum)
                p_cumsum[k] = zs[k, j] = p_sum
            preincrement(u)
            if u == n_rands:
                u = 0
            uval = rands[u] * p_sum
            new_z = topic_lookup[j] = searchsorted(p_cumsum, uval)
            preincrement(ndz[d, new_z])
            preincrement(nzw[new_z, w])
            preincrement(nz[new_z])
            # TODO update zeta, Hznd, HTznd
        # sample eta
        _Z = np.asarray(ndz) / np.asarray(nd)[:, np.newaxis]
        Z = np.empty((n_topics * n_topics, n_edges), dtype=np.float64, order='C')
        for e in range(n_edges):
            Z[:, e] = np.kron(_Z[edge_head[e]], _Z[edge_tail[e]])
        Omega = np.asarray(omega)[np.newaxis, :]
        eta_mean = np.linalg.solve(Inu2 + np.dot(Z * Omega, Z.T),
                                   munu2 + np.dot(Z, kappa))
        # TODO currently setting eta to mean, but need to sample
        for k1 in range(n_topics):
            for k2 in range(n_topics):
                H[i + 1, k1, k2] = eta_mean[k1 + (k2 * n_topics)]
        # compute log-likelihood
        lL[i] = loglikelihood_grtm(nzw, ndz, nz, alpha, beta, sum_beta,
                                   mu, nu2, b, H[i + 1], y, zeta)
        # print progress
        print_progress(start_time, n_report_iter, i, lL[i], lL[i - n_report_iter])
    # populate the topic and word distributions
    theta = estimate_matrix(ndz, alpha, n_docs)
    phi = estimate_matrix(nzw, beta, n_topics)
    return zs, theta, phi, np.asarray(H), np.asarray(lL)